#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'

begin
  doc = Nokogiri::HTML(open("http://www.geometrygames.org/TorusGames/index.html.en", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36"))
rescue
  puts "Failed to fetch download page"
  exit 1
end

link = doc.xpath("//a").find do |link|
  link['href'] && link['href'].match(/Mac/) && link['href'].match(/.zip/)
  
end

puts "http://www.geometrygames.org/TorusGames/#{link['href']}"

